import { Selector, ClientFunction } from 'testcafe';

fixture `EmpikBilety`
    .page `https://empikbilety.develop.k8s-jail.goingdev.goingapp.eu/`
    .httpAuth({
        username: 'wmiasto',
        password: 'g0g0g01ng'
    });

test('Search_NoResults', async t => {
    await t
        .click(Selector('label').withText('Warszawa').find('.Checkbox_selector__OslSL'))
        .click(Selector('label').withText('Dzisiaj').find('.Checkbox_selector__OslSL'))
        .click(Selector('label').withText('Teatr').find('.Checkbox_selector__OslSL'))
        .expect(Selector('body').find('div').find('div').find('div').nth(7).find('div').find('div').nth(6).find('div').nth(1).find('div').nth(144).find('div').find('p').textContent).eql("Brak wyników...");
});

test('MainPage_Elements', async t => {
    await t
        .expect(Selector('#app').find('.EmpikBar_center__ujDgZ').visible).eql(true)
        .expect(Selector('div').withText('Wydarzenia').nth(5).find('.Header_logo__1EXaI[alt="Empik Bilety"]').visible).eql(true)
        .expect(Selector('body').find('div').find('div').find('div').nth(7).find('div').find('div').nth(6).find('div').nth(1).find('div').nth(144).find('div').find('ul').find('li').visible).eql(true)
        .expect(Selector('body').find('div').find('div').find('div').nth(7).find('div').find('div').find('nav').find('div').visible).eql(true)
        .expect(Selector('span').withText('Szukaj').nth(0).visible).eql(true)
        .expect(Selector('div').withText('Wydarzenia').nth(8).nth(8).find('form').find('input').visible).eql(true)
        .expect(Selector('#app').find('button').withText('Pokaż więcej').visible).eql(true)
        .expect(Selector('#app').find('.Footer_footer__2Vqp7').visible).eql(true)
        .expect(Selector('#app').find('.CookieBar_cookieBar__1kP0V').visible).eql(true);
});

test('Search', async t => {
    await t
        .typeText(Selector('.Search_search__2aY_0').find('[name="search"]'), 'happysad')
        .click(Selector('.Search_buttonText__21lmd'))
        .expect(Selector('body').find('div').find('div').find('div').nth(7).find('div').find('div').nth(6).find('div').nth(1).find('div').nth(144).find('div').nth(1).find('ul').find('li').textContent).contains("Happysad");
});

test('Survey', async t => {
    await t
        .expect(Selector('div').withText('Ankieta').visible).eql(true)
        .click(Selector('div').withText('Ankieta'))
        .switchToIframe(Selector('#kampyleForm11427'))
        .expect(Selector('.ng-binding').nth(4).find('p').withText('Dzień dobry,').visible).ok()
        .click(Selector('span').withText('10').nth(2))
        .typeText(Selector('[class^="form-control ng-pristine ng-untouched ng-empty ng-"]'), 'test')
        .click(Selector('button').withText('Dalej'))
        .click(Selector('[class^="ng-pristine ng-untouched ng-empty placeHolderColor"]'))
        .click(Selector('option').withText('Inny'))
        .typeText(Selector('[name="testMe"][class^="form-control ng-pristine ng-untouched ng-empty ng-"]'), 'test')
        .click(Selector('div').withText('Jestem w trakcie :)').nth(10).find('._md-off'))
        .click(Selector('button').withText('Dalej'))
        .click(Selector('div').withText('Nie, nie ma takiej potrzeby').nth(10).find('._md-off'))
        .click(Selector('button').withText('Wyślij'))
        .expect(Selector('.ng-binding').nth(4).nth(4).find('p').find('strong').textContent).eql("Dziękujemy za Twoją opinię!");
});

test('CookiesBar', async t => {
    await t
        .expect(Selector('body').find('div').find('div').find('div').nth(4).visible).eql(true)
        .click(Selector('button').withText('OK'))
        .expect(Selector('body').find('div').find('div').find('div').nth(4).find('div').nth(1).find('button').textContent).notEql("Ok");
});

test('TransactionError', async t => {
    await t
        .typeText(Selector('.Search_search__2aY_0').find('[name="search"]'), 'darmowe')
        .click(Selector('.Search_buttonText__21lmd'))
        .expect(Selector('#app').find('span').withText('od 0 zł').nth(0).textContent).eql("od 0 zł")
        .click(Selector('a').withText('KUP BILET').nth(0))
        .click(Selector('div').withText('KUP BILET').nth(12).nth(12).find('a'))
        .click(Selector('button').withText('1'))
        .typeText(Selector('[name="email"].FormField_input__xTMph'), 'test@test.com')
        .typeText(Selector('[name="firstname"].FormField_input__xTMph'), 'test')
        .typeText(Selector('[name="lastname"].FormField_input__xTMph'), 'test')
        .click(Selector('.Checkbox_selector__3EtwD'))
        .click(Selector('button').withText('KUPUJĘ I PŁACĘ'))
        .expect(Selector('body').find('div').nth(59).find('div').find('div').find('p').textContent).eql("Wystąpił nieoczekiwany błąd podczas zapisu transakcji. Spróbuj ponownie.");
});

test('TransactionSuccess', async t => {
    await t
        .click(Selector('.BuyButton_buyContainer__3aDLM').nth(3).find('a').withText('KUP BILET'))
        .click(Selector('a').withText('KUP BILET'))
        .click(Selector('button').withText('1'))
        .typeText(Selector('[name="email"].FormField_input__xTMph'), 'test@test.com')
        .pressKey('tab')
        .typeText(Selector('[name="firstname"].FormField_input__xTMph'), 'test')
        .pressKey('tab')
        .typeText(Selector('[name="lastname"].FormField_input__xTMph'), 'test')
        .click(Selector('.Checkbox_selector__3EtwD'))
        .click(Selector('button').withText('KUPUJĘ I PŁACĘ'))
        .click(Selector('span').withText('online lub tradycyjny'))
        .typeText(Selector('[name="email"]'), 'test@test.com')
        .click(Selector('.sprite-pbl.m'))
        .click(Selector('#formSubmit'))
        .click(Selector('#formSubmit'))
        .expect(Selector('body').find('div').find('div').find('div').nth(7).find('div').find('div').nth(6).find('div').nth(1).find('div').find('div').nth(2).find('h2').textContent).eql("Dziękujemy za złożenie zamówienia.")
        .expect(Selector('.Event_when__2MEfi').textContent).eql("Czekamy na zaksięgowanie środków.")
        .expect(Selector('.Summary_summary__jMxGx').textContent).eql("Bilet zostanie wysłany na wskazany adres e-mail.Szanujmy drzewa.Nie musisz drukować otrzymanego biletu. Możesz go pokazać na ekranie telefonu.Uwaga!Jeśli Twój bank nie potwierdzi transakcji w ciągu 10 minut, zamówienie może zostać anulowane, o czym poinformujemy mailowo.Jeśli nie otrzymasz od nas żadnej wiadomości w ciągu 24h, skontaktuj się z nami: kontakt@empikbilety.pl");
});

test('TransactionFailed', async t => {
    await t
        .click(Selector('.BuyButton_buyContainer__3aDLM').nth(3).find('a').withText('KUP BILET'))
        .click(Selector('a').withText('KUP BILET'))
        .click(Selector('button').withText('1'))
        .typeText(Selector('[name="email"].FormField_input__xTMph'), 'test@test.com')
        .pressKey('tab')
        .typeText(Selector('[name="firstname"].FormField_input__xTMph'), 'test')
        .pressKey('tab')
        .typeText(Selector('[name="lastname"].FormField_input__xTMph'), 'test')
        .click(Selector('.Checkbox_selector__3EtwD'))
        .click(Selector('button').withText('KUPUJĘ I PŁACĘ'))
        .click(Selector('span').withText('online lub tradycyjny'))
        .typeText(Selector('[name="email"]'), 'test@test.com')
        .click(Selector('.sprite-pbl.m'))
        .click(Selector('#formReturn'))
        .click(Selector('#formSubmit'))
        .expect(Selector('body').find('div').find('div').find('div').nth(7).find('div').find('div').nth(6).find('div').nth(1).find('div').find('div').find('h2').textContent).eql("Transakcja nie powiodła się")
        .expect(Selector('body').find('div').find('div').find('div').nth(7).find('div').find('div').nth(6).find('div').nth(1).find('div').find('div').nth(1).find('div').find('a').textContent).eql("Spróbuj ponownie")
        .click(Selector('a').withText('SPRÓBUJ PONOWNIE'));

    const geturl = ClientFunction(() => {
        return window.location.href;
    });

    await t
        .expect(geturl()).notContains("platnosc-nieudana");
});